// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "KFGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class KF_API AKFGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
