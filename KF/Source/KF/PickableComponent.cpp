// Fill out your copyright notice in the Description page of Project Settings.
#include "PickableComponent.h"
#include "CustomPlayerController.h"
#include "WeaponComponent.h"

void UPickableComponent::CollidedWithActor(AActor* m_actor)
{
	if (!m_actor) return;

	if (!m_actor->ActorHasTag(TEXT("Player"))) return;

	UCustomPlayerController* m_customPlayerController = m_actor->FindComponentByClass<UCustomPlayerController>();

	if (!m_customPlayerController) return;
	if (m_customPlayerController->g_CurrentWeapon) return;
	
	if (!SpawnWeapon(m_actor)) return;

	UWorld* m_world = GetWorld();
	if (!m_world) return;

	AActor* m_owner = GetOwner();
	if (!m_owner) return;

	m_world->DestroyActor(m_owner);
}

AActor* UPickableComponent::SpawnWeapon(AActor* m_actor)
{
	if (!m_actor) return nullptr;
	if (!g_ThisWeaponActor) return nullptr;

	UWorld* m_world = GetWorld();
	AActor* m_owner = GetOwner();

	if (!m_world) return nullptr;
	if (!m_owner) return nullptr;
			   
	UCameraComponent* m_holder = m_actor->FindComponentByClass<UCameraComponent>();
	if (!m_holder) return nullptr;

	FVector m_location = m_holder->GetComponentLocation();
	FRotator m_rotation = FRotator(0.f, -90.f, 0.f);

	AActor* m_weapon = m_world->SpawnActor<ASkeletalMeshActor>(g_ThisWeaponActor, m_location, m_rotation, FActorSpawnParameters());	   
	if (!m_weapon) return nullptr;

	m_weapon->AttachToComponent(m_holder, FAttachmentTransformRules::KeepRelativeTransform);
	m_weapon->SetActorRelativeLocation(FVector(20.f, 32.f, -22.f));

	UCustomPlayerController* m_customPlayerController = m_actor->FindComponentByClass<UCustomPlayerController>();
	if (!m_customPlayerController) return nullptr;

	m_customPlayerController->AddWeaponToInventory(m_weapon);

	UWeaponComponent* WeaponComponent = m_weapon->FindComponentByClass<UWeaponComponent>();
	if (!m_customPlayerController) return nullptr;

	WeaponComponent->Init(g_BulletsCurrent, g_BulletsInMagazineLeft, g_BulletsPerMagazine, g_FireSpeed, g_ReloadTime, m_owner->GetClass());

	return m_weapon;
}

void UPickableComponent::Init(uint16 m_bulletsCurrent, uint16 m_bulletsInMagazineLeft, int m_bulletsPerMagazine, float m_fireSpeed, float m_reloadTime, TSubclassOf<class AActor> m_ThisWeaponActor)
{
	g_BulletsCurrent = m_bulletsCurrent;
	g_BulletsInMagazineLeft = m_bulletsInMagazineLeft;
	g_BulletsPerMagazine = m_bulletsPerMagazine;
	g_FireSpeed = m_fireSpeed;
	g_ReloadTime = m_reloadTime;
	g_ThisWeaponActor = m_ThisWeaponActor;
}
