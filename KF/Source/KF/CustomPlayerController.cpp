// Fill out your copyright notice in the Description page of Project Settings.

#include "CustomPlayerController.h"
#include "MyUserWidget.h"
#include "WeaponComponent.h"
#include "PickableComponent.h"

// Sets default values for this component's properties
UCustomPlayerController::UCustomPlayerController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UCustomPlayerController::BeginPlay()
{
	Super::BeginPlay();

	// ...
	SetupInput();
}

void UCustomPlayerController::SetupInput()
{
	UWorld* m_world = GetWorld();
	if (!m_world) return;

	g_PlayerController = GetWorld()->GetFirstPlayerController();
	if (!g_PlayerController)
	{
		UE_LOG(LogTemp, Error, TEXT("Missing Player Controller!"));
		return;
	}
	
	UInputComponent* m_inputComponent = g_PlayerController->InputComponent;
	if (!m_inputComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("Missing Input Component!"));
		return;
	}

	m_inputComponent->BindAxis("MoveForward", this, &UCustomPlayerController::MoveForward);
	m_inputComponent->BindAxis("MoveRight", this, &UCustomPlayerController::MoveRight);
	m_inputComponent->BindAxis("Turn", this, &UCustomPlayerController::MoveViewportYaw);
	m_inputComponent->BindAxis("LookUp", this, &UCustomPlayerController::MoveViewportPitch);
	m_inputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &UCustomPlayerController::Jump);

	m_inputComponent->BindAction("Fire", EInputEvent::IE_Pressed, this, &UCustomPlayerController::PressWeaponTrigger);
	m_inputComponent->BindAction("Fire", EInputEvent::IE_Released, this, &UCustomPlayerController::ReleaseWeaponTrigger);
	
	m_inputComponent->BindAction("Reload", EInputEvent::IE_Pressed, this, &UCustomPlayerController::ReloadWeapon);
	m_inputComponent->BindAction("Drop", EInputEvent::IE_Pressed, this, &UCustomPlayerController::DropWeapon);

	// DEBUG
	m_inputComponent->BindAction("DebugHurt", EInputEvent::IE_Pressed, this, &UCustomPlayerController::Debug_HurtMe);
	m_inputComponent->BindAction("DebugHeal", EInputEvent::IE_Pressed, this, &UCustomPlayerController::Debug_HealMe);
}

// Called every frame
void UCustomPlayerController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	if(g_TriggerPressed)
		FireWeapon();
}

void UCustomPlayerController::FireWeapon()
{	
	if (!g_CurrentWeapon) return;

	AActor* m_owner = GetOwner();
	if (!m_owner) return;
	
	UCameraComponent* m_playerCamera = m_owner->FindComponentByClass<UCameraComponent>();
	if (!m_playerCamera) return;

	UWeaponComponent* m_currentWeaponComponent = g_CurrentWeapon->FindComponentByClass<UWeaponComponent>();
	if (!m_currentWeaponComponent) return;

	m_currentWeaponComponent->Fire(m_playerCamera);
}

void UCustomPlayerController::PressWeaponTrigger()
{
	g_TriggerPressed = true;
}

void UCustomPlayerController::ReleaseWeaponTrigger()
{
	g_TriggerPressed = false;
}

void UCustomPlayerController::ReloadWeapon()
{
	if (!g_CurrentWeapon) return;

	g_CurrentWeapon->FindComponentByClass<UWeaponComponent>()->Reload();
}

float UCustomPlayerController::GetCurrentHealth()
{
	return g_CurrentHealth;
}

bool UCustomPlayerController::ChangeCurrentHealth(float m_value)
{
	bool m_characterDied = false;

	g_CurrentHealth += m_value;

	if (g_CurrentHealth > g_MaxHealth)
		g_CurrentHealth = g_MaxHealth;

	if (g_CurrentHealth <= 0) {
		g_CurrentHealth = 0;
		m_characterDied = true;
	}

	if (g_GameWidget)
	{
		g_GameWidget->HUDUpdateHealthBar(g_CurrentHealth, g_MaxHealth);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UMyUserWidget reference missing!"));
	}
	
	return m_characterDied;
}

void UCustomPlayerController::DropWeapon()
{
	if (!g_CurrentWeapon) return;

	UWeaponComponent* currentWeaponComponent = g_CurrentWeapon->FindComponentByClass<UWeaponComponent>();
	if (!currentWeaponComponent) return;


	AActor* m_owner = GetOwner();
	if (!m_owner) return;

	UCameraComponent* m_playerCamera = m_owner->FindComponentByClass<UCameraComponent>();
	if (!m_playerCamera) return;

	if (!currentWeaponComponent->CreatePickableWeapon(m_playerCamera)) return;
	
	g_CurrentWeapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);

	UWorld* m_world = GetWorld();
	if (!m_world) return;

	m_world->DestroyActor(g_CurrentWeapon);
	g_CurrentWeapon = nullptr;
}

void UCustomPlayerController::AddWeaponToInventory(AActor* weapon)
{
	if (!weapon) return;

	if (g_CurrentWeapon) return;

	g_CurrentWeapon = weapon;
}

void UCustomPlayerController::MoveForward(float m_scale)
{
	if (!g_PlayerController) return;
		
	APawn* m_playerPawn = g_PlayerController->GetPawn();
	if (!m_playerPawn) return;
	
	m_playerPawn->AddMovementInput(m_playerPawn->GetActorForwardVector() * m_scale);
}

void UCustomPlayerController::MoveRight(float m_scale)
{
	if (!g_PlayerController) return;

	APawn* m_playerPawn = g_PlayerController->GetPawn();
	if (!m_playerPawn) return;

	m_playerPawn->AddMovementInput(m_playerPawn->GetActorRightVector() * m_scale);
}

void UCustomPlayerController::Jump()
{
	if (!g_PlayerController) return;

	ACharacter* m_characterMovementComponent = g_PlayerController->GetCharacter();
	if (!m_characterMovementComponent) return;

	m_characterMovementComponent->Jump();
}


void UCustomPlayerController::MoveViewportYaw(float m_yaw)
{
	if (!g_PlayerController) return;

	APawn* m_playerPawn = g_PlayerController->GetPawn();
	if (!m_playerPawn) return;

	m_playerPawn->AddControllerYawInput(m_yaw);
}

void UCustomPlayerController::MoveViewportPitch(float m_pitch)
{
	if (!g_PlayerController) return;

	APawn* m_playerPawn = g_PlayerController->GetPawn();
	if (!m_playerPawn) return;

	m_playerPawn->AddControllerPitchInput(m_pitch);
}

void UCustomPlayerController::Debug_HurtMe()
{
	ChangeCurrentHealth(-10);
}

void UCustomPlayerController::Debug_HealMe()
{
	ChangeCurrentHealth(10);
}