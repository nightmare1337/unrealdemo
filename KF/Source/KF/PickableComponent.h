// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//
#include "Components/ActorComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Classes/Camera/CameraComponent.h"
#include "Animation/SkeletalMeshActor.h"
#include "Runtime/Engine/Public/EngineUtils.h"
//
#include "CoreMinimal.h"
#include "PickableComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KF_API UPickableComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
	uint16 g_BulletsCurrent = 500;

	UPROPERTY(EditAnywhere)
	uint16 g_BulletsInMagazineLeft = 30;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	int g_BulletsPerMagazine = 30;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	float g_FireSpeed = 10.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	float g_ReloadTime = 1.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
	TSubclassOf<AActor> g_ThisWeaponActor;

public:	
	
	UFUNCTION(BlueprintCallable)
	void CollidedWithActor(AActor* Actor);
	
	UFUNCTION()
	void Init(uint16 m_bulletsCurrent, uint16 m_bulletsInMagazineLeft, int m_bulletsPerMagazine, float m_fireSpeed, float m_reloadTime, TSubclassOf<class AActor> m_ThisWeaponActor);
	
private:

	UFUNCTION()
	AActor* SpawnWeapon(AActor* Actor);
};
