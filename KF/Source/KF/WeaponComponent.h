// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//
#include "Runtime/Engine/Classes/Engine/SkeletalMesh.h"
#include "Components/SkeletalMeshComponent.h"
#include "Animation/SkeletalMeshActor.h"
#include "Runtime/Core/Public/Delegates/Delegate.h"
#include "Classes/Camera/CameraComponent.h"
//
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WeaponComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KF_API UWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
	uint16 g_BulletsCurrent = 500;

	UPROPERTY(EditAnywhere)
	uint16 g_BulletsInMagazineLeft = 30;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
	int g_BulletsPerMagazine = 30;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
	float g_FireSpeed = 10.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
	float g_ReloadTime = 1.5f;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Weapon)
	float g_LastShotTime;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
	float g_TimeSinceReload;
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
	TSubclassOf<AActor> g_ThisWeaponPickableActor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
	TSubclassOf<class ABullet> g_BulletActor;

public:	

	UFUNCTION()
	void Fire(UCameraComponent* m_camera);

	UFUNCTION()
	bool SpawnBullet(UCameraComponent* m_camera);

	UFUNCTION()
	void Reload();

	UFUNCTION()
	bool CreatePickableWeapon(UCameraComponent* m_camera);

	UFUNCTION()
	void Init(uint16 m_bulletsCurrent, uint16 m_bulletsInMagazineLeft, int m_bulletsPerMagazine, float m_fireSpeed, float m_reloadTime, TSubclassOf<class AActor> m_ThisWeaponPickableActor);

};
