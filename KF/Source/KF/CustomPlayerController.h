// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
//
#include "Runtime/Core/Public/Delegates/Delegate.h"
#include "Components/ActorComponent.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Character.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PawnMovementComponent.h"
//
#include "CoreMinimal.h"
#include "CustomPlayerController.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KF_API UCustomPlayerController : public UActorComponent
{
	GENERATED_BODY()
		
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float g_MaxHealth = 100.f;

	// The instance of the players Inventory UI Widget
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
	class UMyUserWidget* g_GameWidget;

	UPROPERTY(EditAnywhere, Category = "Custom")
	AActor* g_CurrentWeapon;

private:
	
	UPROPERTY(VisibleAnywhere)
	float g_CurrentHealth = 1.f;

	UPROPERTY(VisibleAnywhere)
	bool g_TriggerPressed = false;

	UPROPERTY(VisibleAnywhere)
	APlayerController* g_PlayerController;

public:

	UCustomPlayerController();

	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Custom")
	void PressWeaponTrigger();

	UFUNCTION(BlueprintCallable, Category = "Custom")
	void ReleaseWeaponTrigger();

	UFUNCTION(BlueprintCallable, Category = "Custom")
	void FireWeapon();

	UFUNCTION(BlueprintCallable, Category = "Custom")
	void ReloadWeapon();

	UFUNCTION(BlueprintCallable, Category = "Custom")
	void DropWeapon();

	UFUNCTION(BlueprintCallable)
	float GetCurrentHealth();

	UFUNCTION(BlueprintCallable)
	bool ChangeCurrentHealth(float value);

	UFUNCTION(BlueprintCallable)
	void Debug_HurtMe();
	
	UFUNCTION(BlueprintCallable)
	void Debug_HealMe();

	UFUNCTION(BlueprintCallable)
	void AddWeaponToInventory(AActor* Weapon);

	UFUNCTION()
	void MoveForward(float scale);

	UFUNCTION()
	void MoveRight(float scale);

	UFUNCTION()
	void Jump();

	UFUNCTION()
	void MoveViewportYaw(float Yaw);

	UFUNCTION()
	void MoveViewportPitch(float Pitch);

private:

	UFUNCTION()
	void SetupInput();
};
