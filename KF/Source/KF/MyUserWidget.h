// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MyUserWidget.generated.h"


/**
 * 
 */
UCLASS()
class KF_API UMyUserWidget : public UUserWidget
{
	GENERATED_BODY()


	// Called when the game starts
	virtual void NativeConstruct() override;

public:

	UFUNCTION(BlueprintImplementableEvent, Category="HUDRefresh")
	void HUDUpdateHealthBar(float CurrentHealth, float MaxHealth);
};
