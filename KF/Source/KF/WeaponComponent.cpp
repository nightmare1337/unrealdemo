// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponComponent.h"
#include "CustomPlayerController.h"
#include "Bullet.h"
#include "PickableComponent.h"

void UWeaponComponent::Fire(UCameraComponent* m_camera)
{
	if (!g_BulletActor) return;

	UWorld* world = GetWorld();
	if (!world) return;

	if (world->GetTimeSeconds() - g_LastShotTime < 1 / g_FireSpeed) return;
	if (world->GetTimeSeconds() - g_TimeSinceReload < g_ReloadTime) return;
	if(g_BulletsInMagazineLeft <= 0) return;

	g_LastShotTime = world->GetTimeSeconds();
	g_BulletsInMagazineLeft--;
	SpawnBullet(m_camera);
			
	UE_LOG(LogTemp, Warning, TEXT("I SHOT! time: %f bullets left: %d"), g_LastShotTime, g_BulletsInMagazineLeft);
}

bool UWeaponComponent::SpawnBullet(UCameraComponent* m_camera)
{
	if (!m_camera) return nullptr;

	AActor* m_owner = GetOwner();
	UWorld* m_world = GetWorld();

	if (!m_world) return nullptr;
	if (!m_owner) return nullptr;
	
	FActorSpawnParameters m_spawnParams;
	m_spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	m_spawnParams.bNoFail = true;
	m_spawnParams.Owner = m_owner;
	
	FVector m_location = m_owner->GetActorLocation() + m_camera->GetForwardVector() * 100.f;
	FRotator m_rotation = m_camera->GetComponentRotation();	
	ABullet* m_spawnedBullet = m_world->SpawnActor<ABullet>(g_BulletActor, m_location, m_rotation, m_spawnParams);

	return m_spawnedBullet != nullptr;
}

void UWeaponComponent::Reload()
{
	if (g_BulletsInMagazineLeft == g_BulletsPerMagazine) {
		UE_LOG(LogTemp, Warning, TEXT("Magazine full"));
		return;
	}

	if (g_BulletsCurrent == 0) {
		UE_LOG(LogTemp, Warning, TEXT("No bullets left"));
		return;
	}

	g_BulletsCurrent += g_BulletsInMagazineLeft;

	if (g_BulletsCurrent > g_BulletsPerMagazine)
		g_BulletsInMagazineLeft = g_BulletsPerMagazine;
	else
		g_BulletsInMagazineLeft = g_BulletsCurrent;

	g_BulletsCurrent -= g_BulletsInMagazineLeft;

	UE_LOG(LogTemp, Warning, TEXT("I RELOADED WEAPON! bullets left: %d bullets in magazine left: %d"), g_BulletsCurrent, g_BulletsInMagazineLeft);
}

bool UWeaponComponent::CreatePickableWeapon(UCameraComponent* m_camera)
{
	if (!g_ThisWeaponPickableActor) return false;

	UWorld* m_world = GetWorld();
	if (!m_world) return false;

	AActor* m_owner = GetOwner();
	if (!m_owner) return false;
		
	FActorSpawnParameters m_spawnParams;
	m_spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	m_spawnParams.bNoFail = true;
	m_spawnParams.Owner = m_owner;

	FVector m_location = m_owner->GetActorLocation() + m_camera->GetForwardVector() * 100.f;
	FRotator m_rotation = m_camera->GetComponentRotation();

	AActor* m_weapon = m_world->SpawnActor<ASkeletalMeshActor>(g_ThisWeaponPickableActor, m_location, m_rotation, FActorSpawnParameters());
	if (!m_weapon) return false;

	UPickableComponent* m_pickableComponent = m_weapon->FindComponentByClass<UPickableComponent>();
	if (!m_pickableComponent) return false;

	m_pickableComponent->Init(g_BulletsCurrent, g_BulletsInMagazineLeft, g_BulletsPerMagazine, g_FireSpeed, g_ReloadTime, m_owner->GetClass());
	
	g_TimeSinceReload = m_world->GetTimeSeconds();
	return true;
}

void UWeaponComponent::Init(uint16 m_bulletsCurrent, uint16 m_bulletsInMagazineLeft, int m_bulletsPerMagazine, float m_fireSpeed, float m_reloadTime, TSubclassOf<class AActor> m_ThisWeaponPickableActor)
{
	g_BulletsCurrent = m_bulletsCurrent;
	g_BulletsInMagazineLeft = m_bulletsInMagazineLeft;
	g_BulletsPerMagazine = m_bulletsPerMagazine;
	g_FireSpeed = m_fireSpeed;
	g_ReloadTime = m_reloadTime;
	g_ThisWeaponPickableActor = m_ThisWeaponPickableActor;
}