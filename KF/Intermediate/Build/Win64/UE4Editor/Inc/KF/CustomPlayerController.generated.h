// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef KF_CustomPlayerController_generated_h
#error "CustomPlayerController.generated.h already included, missing '#pragma once' in CustomPlayerController.h"
#endif
#define KF_CustomPlayerController_generated_h

#define KF_Source_KF_CustomPlayerController_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetupInput) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetupInput(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveViewportPitch) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Pitch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveViewportPitch(Z_Param_Pitch); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveViewportYaw) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Yaw); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveViewportYaw(Z_Param_Yaw); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execJump) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Jump(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_scale); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveRight(Z_Param_scale); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveForward) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_scale); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveForward(Z_Param_scale); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAddWeaponToInventory) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_Weapon); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AddWeaponToInventory(Z_Param_Weapon); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDebug_HealMe) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Debug_HealMe(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDebug_HurtMe) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Debug_HurtMe(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execChangeCurrentHealth) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ChangeCurrentHealth(Z_Param_value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCurrentHealth(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDropWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DropWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReloadWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ReloadWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFireWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->FireWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReleaseWeaponTrigger) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ReleaseWeaponTrigger(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPressWeaponTrigger) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PressWeaponTrigger(); \
		P_NATIVE_END; \
	}


#define KF_Source_KF_CustomPlayerController_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetupInput) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetupInput(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveViewportPitch) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Pitch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveViewportPitch(Z_Param_Pitch); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveViewportYaw) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Yaw); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveViewportYaw(Z_Param_Yaw); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execJump) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Jump(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_scale); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveRight(Z_Param_scale); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveForward) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_scale); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveForward(Z_Param_scale); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAddWeaponToInventory) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_Weapon); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AddWeaponToInventory(Z_Param_Weapon); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDebug_HealMe) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Debug_HealMe(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDebug_HurtMe) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Debug_HurtMe(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execChangeCurrentHealth) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ChangeCurrentHealth(Z_Param_value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCurrentHealth(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDropWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DropWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReloadWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ReloadWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFireWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->FireWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReleaseWeaponTrigger) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ReleaseWeaponTrigger(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPressWeaponTrigger) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PressWeaponTrigger(); \
		P_NATIVE_END; \
	}


#define KF_Source_KF_CustomPlayerController_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCustomPlayerController(); \
	friend struct Z_Construct_UClass_UCustomPlayerController_Statics; \
public: \
	DECLARE_CLASS(UCustomPlayerController, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/KF"), NO_API) \
	DECLARE_SERIALIZER(UCustomPlayerController)


#define KF_Source_KF_CustomPlayerController_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUCustomPlayerController(); \
	friend struct Z_Construct_UClass_UCustomPlayerController_Statics; \
public: \
	DECLARE_CLASS(UCustomPlayerController, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/KF"), NO_API) \
	DECLARE_SERIALIZER(UCustomPlayerController)


#define KF_Source_KF_CustomPlayerController_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCustomPlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCustomPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCustomPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCustomPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCustomPlayerController(UCustomPlayerController&&); \
	NO_API UCustomPlayerController(const UCustomPlayerController&); \
public:


#define KF_Source_KF_CustomPlayerController_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCustomPlayerController(UCustomPlayerController&&); \
	NO_API UCustomPlayerController(const UCustomPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCustomPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCustomPlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCustomPlayerController)


#define KF_Source_KF_CustomPlayerController_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__g_CurrentHealth() { return STRUCT_OFFSET(UCustomPlayerController, g_CurrentHealth); } \
	FORCEINLINE static uint32 __PPO__g_TriggerPressed() { return STRUCT_OFFSET(UCustomPlayerController, g_TriggerPressed); } \
	FORCEINLINE static uint32 __PPO__g_PlayerController() { return STRUCT_OFFSET(UCustomPlayerController, g_PlayerController); }


#define KF_Source_KF_CustomPlayerController_h_17_PROLOG
#define KF_Source_KF_CustomPlayerController_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	KF_Source_KF_CustomPlayerController_h_20_PRIVATE_PROPERTY_OFFSET \
	KF_Source_KF_CustomPlayerController_h_20_RPC_WRAPPERS \
	KF_Source_KF_CustomPlayerController_h_20_INCLASS \
	KF_Source_KF_CustomPlayerController_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define KF_Source_KF_CustomPlayerController_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	KF_Source_KF_CustomPlayerController_h_20_PRIVATE_PROPERTY_OFFSET \
	KF_Source_KF_CustomPlayerController_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	KF_Source_KF_CustomPlayerController_h_20_INCLASS_NO_PURE_DECLS \
	KF_Source_KF_CustomPlayerController_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> KF_API UClass* StaticClass<class UCustomPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID KF_Source_KF_CustomPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
