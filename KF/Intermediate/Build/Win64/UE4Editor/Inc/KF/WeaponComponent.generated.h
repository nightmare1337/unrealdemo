// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UCameraComponent;
#ifdef KF_WeaponComponent_generated_h
#error "WeaponComponent.generated.h already included, missing '#pragma once' in WeaponComponent.h"
#endif
#define KF_WeaponComponent_generated_h

#define KF_Source_KF_WeaponComponent_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInit) \
	{ \
		P_GET_PROPERTY(UUInt16Property,Z_Param_m_bulletsCurrent); \
		P_GET_PROPERTY(UUInt16Property,Z_Param_m_bulletsInMagazineLeft); \
		P_GET_PROPERTY(UIntProperty,Z_Param_m_bulletsPerMagazine); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_m_fireSpeed); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_m_reloadTime); \
		P_GET_OBJECT(UClass,Z_Param_m_ThisWeaponPickableActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Init(Z_Param_m_bulletsCurrent,Z_Param_m_bulletsInMagazineLeft,Z_Param_m_bulletsPerMagazine,Z_Param_m_fireSpeed,Z_Param_m_reloadTime,Z_Param_m_ThisWeaponPickableActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreatePickableWeapon) \
	{ \
		P_GET_OBJECT(UCameraComponent,Z_Param_m_camera); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->CreatePickableWeapon(Z_Param_m_camera); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReload) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Reload(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpawnBullet) \
	{ \
		P_GET_OBJECT(UCameraComponent,Z_Param_m_camera); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->SpawnBullet(Z_Param_m_camera); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFire) \
	{ \
		P_GET_OBJECT(UCameraComponent,Z_Param_m_camera); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Fire(Z_Param_m_camera); \
		P_NATIVE_END; \
	}


#define KF_Source_KF_WeaponComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInit) \
	{ \
		P_GET_PROPERTY(UUInt16Property,Z_Param_m_bulletsCurrent); \
		P_GET_PROPERTY(UUInt16Property,Z_Param_m_bulletsInMagazineLeft); \
		P_GET_PROPERTY(UIntProperty,Z_Param_m_bulletsPerMagazine); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_m_fireSpeed); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_m_reloadTime); \
		P_GET_OBJECT(UClass,Z_Param_m_ThisWeaponPickableActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Init(Z_Param_m_bulletsCurrent,Z_Param_m_bulletsInMagazineLeft,Z_Param_m_bulletsPerMagazine,Z_Param_m_fireSpeed,Z_Param_m_reloadTime,Z_Param_m_ThisWeaponPickableActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreatePickableWeapon) \
	{ \
		P_GET_OBJECT(UCameraComponent,Z_Param_m_camera); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->CreatePickableWeapon(Z_Param_m_camera); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReload) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Reload(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpawnBullet) \
	{ \
		P_GET_OBJECT(UCameraComponent,Z_Param_m_camera); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->SpawnBullet(Z_Param_m_camera); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFire) \
	{ \
		P_GET_OBJECT(UCameraComponent,Z_Param_m_camera); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Fire(Z_Param_m_camera); \
		P_NATIVE_END; \
	}


#define KF_Source_KF_WeaponComponent_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWeaponComponent(); \
	friend struct Z_Construct_UClass_UWeaponComponent_Statics; \
public: \
	DECLARE_CLASS(UWeaponComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/KF"), NO_API) \
	DECLARE_SERIALIZER(UWeaponComponent)


#define KF_Source_KF_WeaponComponent_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUWeaponComponent(); \
	friend struct Z_Construct_UClass_UWeaponComponent_Statics; \
public: \
	DECLARE_CLASS(UWeaponComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/KF"), NO_API) \
	DECLARE_SERIALIZER(UWeaponComponent)


#define KF_Source_KF_WeaponComponent_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWeaponComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWeaponComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWeaponComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWeaponComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWeaponComponent(UWeaponComponent&&); \
	NO_API UWeaponComponent(const UWeaponComponent&); \
public:


#define KF_Source_KF_WeaponComponent_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWeaponComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWeaponComponent(UWeaponComponent&&); \
	NO_API UWeaponComponent(const UWeaponComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWeaponComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWeaponComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWeaponComponent)


#define KF_Source_KF_WeaponComponent_h_19_PRIVATE_PROPERTY_OFFSET
#define KF_Source_KF_WeaponComponent_h_16_PROLOG
#define KF_Source_KF_WeaponComponent_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	KF_Source_KF_WeaponComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	KF_Source_KF_WeaponComponent_h_19_RPC_WRAPPERS \
	KF_Source_KF_WeaponComponent_h_19_INCLASS \
	KF_Source_KF_WeaponComponent_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define KF_Source_KF_WeaponComponent_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	KF_Source_KF_WeaponComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	KF_Source_KF_WeaponComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	KF_Source_KF_WeaponComponent_h_19_INCLASS_NO_PURE_DECLS \
	KF_Source_KF_WeaponComponent_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> KF_API UClass* StaticClass<class UWeaponComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID KF_Source_KF_WeaponComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
