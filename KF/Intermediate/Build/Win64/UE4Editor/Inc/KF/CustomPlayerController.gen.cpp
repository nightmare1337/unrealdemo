// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "KF/CustomPlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCustomPlayerController() {}
// Cross Module References
	KF_API UClass* Z_Construct_UClass_UCustomPlayerController_NoRegister();
	KF_API UClass* Z_Construct_UClass_UCustomPlayerController();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_KF();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_Debug_HealMe();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_Debug_HurtMe();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_DropWeapon();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_FireWeapon();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_Jump();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_MoveForward();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_MoveRight();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_PressWeaponTrigger();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_ReleaseWeaponTrigger();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_ReloadWeapon();
	KF_API UFunction* Z_Construct_UFunction_UCustomPlayerController_SetupInput();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	KF_API UClass* Z_Construct_UClass_UMyUserWidget_NoRegister();
// End Cross Module References
	void UCustomPlayerController::StaticRegisterNativesUCustomPlayerController()
	{
		UClass* Class = UCustomPlayerController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddWeaponToInventory", &UCustomPlayerController::execAddWeaponToInventory },
			{ "ChangeCurrentHealth", &UCustomPlayerController::execChangeCurrentHealth },
			{ "Debug_HealMe", &UCustomPlayerController::execDebug_HealMe },
			{ "Debug_HurtMe", &UCustomPlayerController::execDebug_HurtMe },
			{ "DropWeapon", &UCustomPlayerController::execDropWeapon },
			{ "FireWeapon", &UCustomPlayerController::execFireWeapon },
			{ "GetCurrentHealth", &UCustomPlayerController::execGetCurrentHealth },
			{ "Jump", &UCustomPlayerController::execJump },
			{ "MoveForward", &UCustomPlayerController::execMoveForward },
			{ "MoveRight", &UCustomPlayerController::execMoveRight },
			{ "MoveViewportPitch", &UCustomPlayerController::execMoveViewportPitch },
			{ "MoveViewportYaw", &UCustomPlayerController::execMoveViewportYaw },
			{ "PressWeaponTrigger", &UCustomPlayerController::execPressWeaponTrigger },
			{ "ReleaseWeaponTrigger", &UCustomPlayerController::execReleaseWeaponTrigger },
			{ "ReloadWeapon", &UCustomPlayerController::execReloadWeapon },
			{ "SetupInput", &UCustomPlayerController::execSetupInput },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory_Statics
	{
		struct CustomPlayerController_eventAddWeaponToInventory_Parms
		{
			AActor* Weapon;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Weapon;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory_Statics::NewProp_Weapon = { "Weapon", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CustomPlayerController_eventAddWeaponToInventory_Parms, Weapon), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory_Statics::NewProp_Weapon,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "AddWeaponToInventory", sizeof(CustomPlayerController_eventAddWeaponToInventory_Parms), Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics
	{
		struct CustomPlayerController_eventChangeCurrentHealth_Parms
		{
			float value;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CustomPlayerController_eventChangeCurrentHealth_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CustomPlayerController_eventChangeCurrentHealth_Parms), &Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::NewProp_value = { "value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CustomPlayerController_eventChangeCurrentHealth_Parms, value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::NewProp_value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "ChangeCurrentHealth", sizeof(CustomPlayerController_eventChangeCurrentHealth_Parms), Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_Debug_HealMe_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_Debug_HealMe_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_Debug_HealMe_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "Debug_HealMe", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_Debug_HealMe_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_Debug_HealMe_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_Debug_HealMe()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_Debug_HealMe_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_Debug_HurtMe_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_Debug_HurtMe_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_Debug_HurtMe_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "Debug_HurtMe", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_Debug_HurtMe_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_Debug_HurtMe_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_Debug_HurtMe()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_Debug_HurtMe_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_DropWeapon_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_DropWeapon_Statics::Function_MetaDataParams[] = {
		{ "Category", "Custom" },
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_DropWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "DropWeapon", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_DropWeapon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_DropWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_DropWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_DropWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_FireWeapon_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_FireWeapon_Statics::Function_MetaDataParams[] = {
		{ "Category", "Custom" },
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_FireWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "FireWeapon", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_FireWeapon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_FireWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_FireWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_FireWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth_Statics
	{
		struct CustomPlayerController_eventGetCurrentHealth_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CustomPlayerController_eventGetCurrentHealth_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "GetCurrentHealth", sizeof(CustomPlayerController_eventGetCurrentHealth_Parms), Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_Jump_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_Jump_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_Jump_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "Jump", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_Jump_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_Jump_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_Jump()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_Jump_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_MoveForward_Statics
	{
		struct CustomPlayerController_eventMoveForward_Parms
		{
			float scale;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_scale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCustomPlayerController_MoveForward_Statics::NewProp_scale = { "scale", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CustomPlayerController_eventMoveForward_Parms, scale), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCustomPlayerController_MoveForward_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCustomPlayerController_MoveForward_Statics::NewProp_scale,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_MoveForward_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_MoveForward_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "MoveForward", sizeof(CustomPlayerController_eventMoveForward_Parms), Z_Construct_UFunction_UCustomPlayerController_MoveForward_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_MoveForward_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_MoveForward_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_MoveForward_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_MoveForward()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_MoveForward_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_MoveRight_Statics
	{
		struct CustomPlayerController_eventMoveRight_Parms
		{
			float scale;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_scale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCustomPlayerController_MoveRight_Statics::NewProp_scale = { "scale", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CustomPlayerController_eventMoveRight_Parms, scale), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCustomPlayerController_MoveRight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCustomPlayerController_MoveRight_Statics::NewProp_scale,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_MoveRight_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_MoveRight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "MoveRight", sizeof(CustomPlayerController_eventMoveRight_Parms), Z_Construct_UFunction_UCustomPlayerController_MoveRight_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_MoveRight_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_MoveRight_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_MoveRight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_MoveRight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_MoveRight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch_Statics
	{
		struct CustomPlayerController_eventMoveViewportPitch_Parms
		{
			float Pitch;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Pitch;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch_Statics::NewProp_Pitch = { "Pitch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CustomPlayerController_eventMoveViewportPitch_Parms, Pitch), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch_Statics::NewProp_Pitch,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "MoveViewportPitch", sizeof(CustomPlayerController_eventMoveViewportPitch_Parms), Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw_Statics
	{
		struct CustomPlayerController_eventMoveViewportYaw_Parms
		{
			float Yaw;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Yaw;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw_Statics::NewProp_Yaw = { "Yaw", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CustomPlayerController_eventMoveViewportYaw_Parms, Yaw), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw_Statics::NewProp_Yaw,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "MoveViewportYaw", sizeof(CustomPlayerController_eventMoveViewportYaw_Parms), Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_PressWeaponTrigger_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_PressWeaponTrigger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Custom" },
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_PressWeaponTrigger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "PressWeaponTrigger", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_PressWeaponTrigger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_PressWeaponTrigger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_PressWeaponTrigger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_PressWeaponTrigger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_ReleaseWeaponTrigger_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_ReleaseWeaponTrigger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Custom" },
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_ReleaseWeaponTrigger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "ReleaseWeaponTrigger", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_ReleaseWeaponTrigger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_ReleaseWeaponTrigger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_ReleaseWeaponTrigger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_ReleaseWeaponTrigger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_ReloadWeapon_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_ReloadWeapon_Statics::Function_MetaDataParams[] = {
		{ "Category", "Custom" },
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_ReloadWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "ReloadWeapon", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_ReloadWeapon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_ReloadWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_ReloadWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_ReloadWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCustomPlayerController_SetupInput_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCustomPlayerController_SetupInput_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCustomPlayerController_SetupInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCustomPlayerController, nullptr, "SetupInput", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCustomPlayerController_SetupInput_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCustomPlayerController_SetupInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCustomPlayerController_SetupInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCustomPlayerController_SetupInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCustomPlayerController_NoRegister()
	{
		return UCustomPlayerController::StaticClass();
	}
	struct Z_Construct_UClass_UCustomPlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_PlayerController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_g_PlayerController;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_TriggerPressed_MetaData[];
#endif
		static void NewProp_g_TriggerPressed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_g_TriggerPressed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_CurrentHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_g_CurrentHealth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_CurrentWeapon_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_g_CurrentWeapon;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_GameWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_g_GameWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_MaxHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_g_MaxHealth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCustomPlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_KF,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCustomPlayerController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCustomPlayerController_AddWeaponToInventory, "AddWeaponToInventory" }, // 3643862025
		{ &Z_Construct_UFunction_UCustomPlayerController_ChangeCurrentHealth, "ChangeCurrentHealth" }, // 2440554259
		{ &Z_Construct_UFunction_UCustomPlayerController_Debug_HealMe, "Debug_HealMe" }, // 2658555408
		{ &Z_Construct_UFunction_UCustomPlayerController_Debug_HurtMe, "Debug_HurtMe" }, // 2212813035
		{ &Z_Construct_UFunction_UCustomPlayerController_DropWeapon, "DropWeapon" }, // 1777599382
		{ &Z_Construct_UFunction_UCustomPlayerController_FireWeapon, "FireWeapon" }, // 2577242191
		{ &Z_Construct_UFunction_UCustomPlayerController_GetCurrentHealth, "GetCurrentHealth" }, // 1918140105
		{ &Z_Construct_UFunction_UCustomPlayerController_Jump, "Jump" }, // 2669321176
		{ &Z_Construct_UFunction_UCustomPlayerController_MoveForward, "MoveForward" }, // 3286730549
		{ &Z_Construct_UFunction_UCustomPlayerController_MoveRight, "MoveRight" }, // 1402104679
		{ &Z_Construct_UFunction_UCustomPlayerController_MoveViewportPitch, "MoveViewportPitch" }, // 3791070090
		{ &Z_Construct_UFunction_UCustomPlayerController_MoveViewportYaw, "MoveViewportYaw" }, // 3208056604
		{ &Z_Construct_UFunction_UCustomPlayerController_PressWeaponTrigger, "PressWeaponTrigger" }, // 1107481592
		{ &Z_Construct_UFunction_UCustomPlayerController_ReleaseWeaponTrigger, "ReleaseWeaponTrigger" }, // 1732059487
		{ &Z_Construct_UFunction_UCustomPlayerController_ReloadWeapon, "ReloadWeapon" }, // 2098408306
		{ &Z_Construct_UFunction_UCustomPlayerController_SetupInput, "SetupInput" }, // 595114189
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCustomPlayerController_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "CustomPlayerController.h" },
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_PlayerController_MetaData[] = {
		{ "Category", "CustomPlayerController" },
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_PlayerController = { "g_PlayerController", nullptr, (EPropertyFlags)0x0040000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCustomPlayerController, g_PlayerController), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_PlayerController_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_PlayerController_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_TriggerPressed_MetaData[] = {
		{ "Category", "CustomPlayerController" },
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	void Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_TriggerPressed_SetBit(void* Obj)
	{
		((UCustomPlayerController*)Obj)->g_TriggerPressed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_TriggerPressed = { "g_TriggerPressed", nullptr, (EPropertyFlags)0x0040000000020001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCustomPlayerController), &Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_TriggerPressed_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_TriggerPressed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_TriggerPressed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_CurrentHealth_MetaData[] = {
		{ "Category", "CustomPlayerController" },
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_CurrentHealth = { "g_CurrentHealth", nullptr, (EPropertyFlags)0x0040000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCustomPlayerController, g_CurrentHealth), METADATA_PARAMS(Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_CurrentHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_CurrentHealth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_CurrentWeapon_MetaData[] = {
		{ "Category", "Custom" },
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_CurrentWeapon = { "g_CurrentWeapon", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCustomPlayerController, g_CurrentWeapon), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_CurrentWeapon_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_CurrentWeapon_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_GameWidget_MetaData[] = {
		{ "Category", "HUD" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "CustomPlayerController.h" },
		{ "ToolTip", "The instance of the players Inventory UI Widget" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_GameWidget = { "g_GameWidget", nullptr, (EPropertyFlags)0x001000000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCustomPlayerController, g_GameWidget), Z_Construct_UClass_UMyUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_GameWidget_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_GameWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_MaxHealth_MetaData[] = {
		{ "Category", "CustomPlayerController" },
		{ "ModuleRelativePath", "CustomPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_MaxHealth = { "g_MaxHealth", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCustomPlayerController, g_MaxHealth), METADATA_PARAMS(Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_MaxHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_MaxHealth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCustomPlayerController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_PlayerController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_TriggerPressed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_CurrentHealth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_CurrentWeapon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_GameWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCustomPlayerController_Statics::NewProp_g_MaxHealth,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCustomPlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCustomPlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCustomPlayerController_Statics::ClassParams = {
		&UCustomPlayerController::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCustomPlayerController_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UCustomPlayerController_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UCustomPlayerController_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UCustomPlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCustomPlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCustomPlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCustomPlayerController, 898079145);
	template<> KF_API UClass* StaticClass<UCustomPlayerController>()
	{
		return UCustomPlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCustomPlayerController(Z_Construct_UClass_UCustomPlayerController, &UCustomPlayerController::StaticClass, TEXT("/Script/KF"), TEXT("UCustomPlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCustomPlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
