// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef KF_KFGameModeBase_generated_h
#error "KFGameModeBase.generated.h already included, missing '#pragma once' in KFGameModeBase.h"
#endif
#define KF_KFGameModeBase_generated_h

#define KF_Source_KF_KFGameModeBase_h_15_RPC_WRAPPERS
#define KF_Source_KF_KFGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define KF_Source_KF_KFGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAKFGameModeBase(); \
	friend struct Z_Construct_UClass_AKFGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AKFGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/KF"), NO_API) \
	DECLARE_SERIALIZER(AKFGameModeBase)


#define KF_Source_KF_KFGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAKFGameModeBase(); \
	friend struct Z_Construct_UClass_AKFGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AKFGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/KF"), NO_API) \
	DECLARE_SERIALIZER(AKFGameModeBase)


#define KF_Source_KF_KFGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AKFGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AKFGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AKFGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AKFGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AKFGameModeBase(AKFGameModeBase&&); \
	NO_API AKFGameModeBase(const AKFGameModeBase&); \
public:


#define KF_Source_KF_KFGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AKFGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AKFGameModeBase(AKFGameModeBase&&); \
	NO_API AKFGameModeBase(const AKFGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AKFGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AKFGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AKFGameModeBase)


#define KF_Source_KF_KFGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define KF_Source_KF_KFGameModeBase_h_12_PROLOG
#define KF_Source_KF_KFGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	KF_Source_KF_KFGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	KF_Source_KF_KFGameModeBase_h_15_RPC_WRAPPERS \
	KF_Source_KF_KFGameModeBase_h_15_INCLASS \
	KF_Source_KF_KFGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define KF_Source_KF_KFGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	KF_Source_KF_KFGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	KF_Source_KF_KFGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	KF_Source_KF_KFGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	KF_Source_KF_KFGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> KF_API UClass* StaticClass<class AKFGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID KF_Source_KF_KFGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
