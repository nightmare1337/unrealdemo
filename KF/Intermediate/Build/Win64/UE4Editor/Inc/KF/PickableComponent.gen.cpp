// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "KF/PickableComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePickableComponent() {}
// Cross Module References
	KF_API UClass* Z_Construct_UClass_UPickableComponent_NoRegister();
	KF_API UClass* Z_Construct_UClass_UPickableComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_KF();
	KF_API UFunction* Z_Construct_UFunction_UPickableComponent_CollidedWithActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	KF_API UFunction* Z_Construct_UFunction_UPickableComponent_Init();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	KF_API UFunction* Z_Construct_UFunction_UPickableComponent_SpawnWeapon();
// End Cross Module References
	void UPickableComponent::StaticRegisterNativesUPickableComponent()
	{
		UClass* Class = UPickableComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CollidedWithActor", &UPickableComponent::execCollidedWithActor },
			{ "Init", &UPickableComponent::execInit },
			{ "SpawnWeapon", &UPickableComponent::execSpawnWeapon },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPickableComponent_CollidedWithActor_Statics
	{
		struct PickableComponent_eventCollidedWithActor_Parms
		{
			AActor* Actor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPickableComponent_CollidedWithActor_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickableComponent_eventCollidedWithActor_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPickableComponent_CollidedWithActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPickableComponent_CollidedWithActor_Statics::NewProp_Actor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPickableComponent_CollidedWithActor_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PickableComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPickableComponent_CollidedWithActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPickableComponent, nullptr, "CollidedWithActor", sizeof(PickableComponent_eventCollidedWithActor_Parms), Z_Construct_UFunction_UPickableComponent_CollidedWithActor_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPickableComponent_CollidedWithActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPickableComponent_CollidedWithActor_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPickableComponent_CollidedWithActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPickableComponent_CollidedWithActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPickableComponent_CollidedWithActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPickableComponent_Init_Statics
	{
		struct PickableComponent_eventInit_Parms
		{
			uint16 m_bulletsCurrent;
			uint16 m_bulletsInMagazineLeft;
			int32 m_bulletsPerMagazine;
			float m_fireSpeed;
			float m_reloadTime;
			TSubclassOf<AActor>  m_ThisWeaponActor;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_m_ThisWeaponActor;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_m_reloadTime;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_m_fireSpeed;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_m_bulletsPerMagazine;
		static const UE4CodeGen_Private::FUInt16PropertyParams NewProp_m_bulletsInMagazineLeft;
		static const UE4CodeGen_Private::FUInt16PropertyParams NewProp_m_bulletsCurrent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UPickableComponent_Init_Statics::NewProp_m_ThisWeaponActor = { "m_ThisWeaponActor", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickableComponent_eventInit_Parms, m_ThisWeaponActor), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UPickableComponent_Init_Statics::NewProp_m_reloadTime = { "m_reloadTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickableComponent_eventInit_Parms, m_reloadTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UPickableComponent_Init_Statics::NewProp_m_fireSpeed = { "m_fireSpeed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickableComponent_eventInit_Parms, m_fireSpeed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPickableComponent_Init_Statics::NewProp_m_bulletsPerMagazine = { "m_bulletsPerMagazine", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickableComponent_eventInit_Parms, m_bulletsPerMagazine), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUInt16PropertyParams Z_Construct_UFunction_UPickableComponent_Init_Statics::NewProp_m_bulletsInMagazineLeft = { "m_bulletsInMagazineLeft", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickableComponent_eventInit_Parms, m_bulletsInMagazineLeft), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUInt16PropertyParams Z_Construct_UFunction_UPickableComponent_Init_Statics::NewProp_m_bulletsCurrent = { "m_bulletsCurrent", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickableComponent_eventInit_Parms, m_bulletsCurrent), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPickableComponent_Init_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPickableComponent_Init_Statics::NewProp_m_ThisWeaponActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPickableComponent_Init_Statics::NewProp_m_reloadTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPickableComponent_Init_Statics::NewProp_m_fireSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPickableComponent_Init_Statics::NewProp_m_bulletsPerMagazine,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPickableComponent_Init_Statics::NewProp_m_bulletsInMagazineLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPickableComponent_Init_Statics::NewProp_m_bulletsCurrent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPickableComponent_Init_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PickableComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPickableComponent_Init_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPickableComponent, nullptr, "Init", sizeof(PickableComponent_eventInit_Parms), Z_Construct_UFunction_UPickableComponent_Init_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPickableComponent_Init_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPickableComponent_Init_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPickableComponent_Init_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPickableComponent_Init()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPickableComponent_Init_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics
	{
		struct PickableComponent_eventSpawnWeapon_Parms
		{
			AActor* Actor;
			AActor* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickableComponent_eventSpawnWeapon_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PickableComponent_eventSpawnWeapon_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics::NewProp_Actor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PickableComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPickableComponent, nullptr, "SpawnWeapon", sizeof(PickableComponent_eventSpawnWeapon_Parms), Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPickableComponent_SpawnWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPickableComponent_SpawnWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPickableComponent_NoRegister()
	{
		return UPickableComponent::StaticClass();
	}
	struct Z_Construct_UClass_UPickableComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_ThisWeaponActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_g_ThisWeaponActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_ReloadTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_g_ReloadTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_FireSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_g_FireSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_BulletsPerMagazine_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_g_BulletsPerMagazine;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_BulletsInMagazineLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt16PropertyParams NewProp_g_BulletsInMagazineLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_BulletsCurrent_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt16PropertyParams NewProp_g_BulletsCurrent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPickableComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_KF,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPickableComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPickableComponent_CollidedWithActor, "CollidedWithActor" }, // 1407529752
		{ &Z_Construct_UFunction_UPickableComponent_Init, "Init" }, // 1644826138
		{ &Z_Construct_UFunction_UPickableComponent_SpawnWeapon, "SpawnWeapon" }, // 872753931
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPickableComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "PickableComponent.h" },
		{ "ModuleRelativePath", "PickableComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_ThisWeaponActor_MetaData[] = {
		{ "Category", "Weapons" },
		{ "ModuleRelativePath", "PickableComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_ThisWeaponActor = { "g_ThisWeaponActor", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPickableComponent, g_ThisWeaponActor), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_ThisWeaponActor_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_ThisWeaponActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_ReloadTime_MetaData[] = {
		{ "Category", "Weapon" },
		{ "ModuleRelativePath", "PickableComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_ReloadTime = { "g_ReloadTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPickableComponent, g_ReloadTime), METADATA_PARAMS(Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_ReloadTime_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_ReloadTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_FireSpeed_MetaData[] = {
		{ "Category", "Weapon" },
		{ "ModuleRelativePath", "PickableComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_FireSpeed = { "g_FireSpeed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPickableComponent, g_FireSpeed), METADATA_PARAMS(Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_FireSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_FireSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsPerMagazine_MetaData[] = {
		{ "Category", "Weapon" },
		{ "ModuleRelativePath", "PickableComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsPerMagazine = { "g_BulletsPerMagazine", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPickableComponent, g_BulletsPerMagazine), METADATA_PARAMS(Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsPerMagazine_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsPerMagazine_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsInMagazineLeft_MetaData[] = {
		{ "Category", "PickableComponent" },
		{ "ModuleRelativePath", "PickableComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt16PropertyParams Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsInMagazineLeft = { "g_BulletsInMagazineLeft", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPickableComponent, g_BulletsInMagazineLeft), METADATA_PARAMS(Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsInMagazineLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsInMagazineLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsCurrent_MetaData[] = {
		{ "Category", "PickableComponent" },
		{ "ModuleRelativePath", "PickableComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt16PropertyParams Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsCurrent = { "g_BulletsCurrent", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPickableComponent, g_BulletsCurrent), METADATA_PARAMS(Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsCurrent_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsCurrent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPickableComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_ThisWeaponActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_ReloadTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_FireSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsPerMagazine,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsInMagazineLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPickableComponent_Statics::NewProp_g_BulletsCurrent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPickableComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPickableComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPickableComponent_Statics::ClassParams = {
		&UPickableComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPickableComponent_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UPickableComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPickableComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UPickableComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPickableComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPickableComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPickableComponent, 1145737176);
	template<> KF_API UClass* StaticClass<UPickableComponent>()
	{
		return UPickableComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPickableComponent(Z_Construct_UClass_UPickableComponent, &UPickableComponent::StaticClass, TEXT("/Script/KF"), TEXT("UPickableComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPickableComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
