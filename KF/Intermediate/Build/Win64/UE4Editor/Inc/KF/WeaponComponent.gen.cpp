// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "KF/WeaponComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWeaponComponent() {}
// Cross Module References
	KF_API UClass* Z_Construct_UClass_UWeaponComponent_NoRegister();
	KF_API UClass* Z_Construct_UClass_UWeaponComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_KF();
	KF_API UFunction* Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	KF_API UFunction* Z_Construct_UFunction_UWeaponComponent_Fire();
	KF_API UFunction* Z_Construct_UFunction_UWeaponComponent_Init();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	KF_API UFunction* Z_Construct_UFunction_UWeaponComponent_Reload();
	KF_API UFunction* Z_Construct_UFunction_UWeaponComponent_SpawnBullet();
	KF_API UClass* Z_Construct_UClass_ABullet_NoRegister();
// End Cross Module References
	void UWeaponComponent::StaticRegisterNativesUWeaponComponent()
	{
		UClass* Class = UWeaponComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreatePickableWeapon", &UWeaponComponent::execCreatePickableWeapon },
			{ "Fire", &UWeaponComponent::execFire },
			{ "Init", &UWeaponComponent::execInit },
			{ "Reload", &UWeaponComponent::execReload },
			{ "SpawnBullet", &UWeaponComponent::execSpawnBullet },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics
	{
		struct WeaponComponent_eventCreatePickableWeapon_Parms
		{
			UCameraComponent* m_camera;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_camera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_camera;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WeaponComponent_eventCreatePickableWeapon_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WeaponComponent_eventCreatePickableWeapon_Parms), &Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::NewProp_m_camera_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::NewProp_m_camera = { "m_camera", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponComponent_eventCreatePickableWeapon_Parms, m_camera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::NewProp_m_camera_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::NewProp_m_camera_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::NewProp_m_camera,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWeaponComponent, nullptr, "CreatePickableWeapon", sizeof(WeaponComponent_eventCreatePickableWeapon_Parms), Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWeaponComponent_Fire_Statics
	{
		struct WeaponComponent_eventFire_Parms
		{
			UCameraComponent* m_camera;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_camera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_camera;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWeaponComponent_Fire_Statics::NewProp_m_camera_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UWeaponComponent_Fire_Statics::NewProp_m_camera = { "m_camera", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponComponent_eventFire_Parms, m_camera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UWeaponComponent_Fire_Statics::NewProp_m_camera_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UWeaponComponent_Fire_Statics::NewProp_m_camera_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWeaponComponent_Fire_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWeaponComponent_Fire_Statics::NewProp_m_camera,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWeaponComponent_Fire_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWeaponComponent_Fire_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWeaponComponent, nullptr, "Fire", sizeof(WeaponComponent_eventFire_Parms), Z_Construct_UFunction_UWeaponComponent_Fire_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UWeaponComponent_Fire_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWeaponComponent_Fire_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UWeaponComponent_Fire_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWeaponComponent_Fire()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWeaponComponent_Fire_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWeaponComponent_Init_Statics
	{
		struct WeaponComponent_eventInit_Parms
		{
			uint16 m_bulletsCurrent;
			uint16 m_bulletsInMagazineLeft;
			int32 m_bulletsPerMagazine;
			float m_fireSpeed;
			float m_reloadTime;
			TSubclassOf<AActor>  m_ThisWeaponPickableActor;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_m_ThisWeaponPickableActor;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_m_reloadTime;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_m_fireSpeed;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_m_bulletsPerMagazine;
		static const UE4CodeGen_Private::FUInt16PropertyParams NewProp_m_bulletsInMagazineLeft;
		static const UE4CodeGen_Private::FUInt16PropertyParams NewProp_m_bulletsCurrent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UWeaponComponent_Init_Statics::NewProp_m_ThisWeaponPickableActor = { "m_ThisWeaponPickableActor", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponComponent_eventInit_Parms, m_ThisWeaponPickableActor), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UWeaponComponent_Init_Statics::NewProp_m_reloadTime = { "m_reloadTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponComponent_eventInit_Parms, m_reloadTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UWeaponComponent_Init_Statics::NewProp_m_fireSpeed = { "m_fireSpeed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponComponent_eventInit_Parms, m_fireSpeed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UWeaponComponent_Init_Statics::NewProp_m_bulletsPerMagazine = { "m_bulletsPerMagazine", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponComponent_eventInit_Parms, m_bulletsPerMagazine), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUInt16PropertyParams Z_Construct_UFunction_UWeaponComponent_Init_Statics::NewProp_m_bulletsInMagazineLeft = { "m_bulletsInMagazineLeft", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponComponent_eventInit_Parms, m_bulletsInMagazineLeft), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUInt16PropertyParams Z_Construct_UFunction_UWeaponComponent_Init_Statics::NewProp_m_bulletsCurrent = { "m_bulletsCurrent", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponComponent_eventInit_Parms, m_bulletsCurrent), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWeaponComponent_Init_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWeaponComponent_Init_Statics::NewProp_m_ThisWeaponPickableActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWeaponComponent_Init_Statics::NewProp_m_reloadTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWeaponComponent_Init_Statics::NewProp_m_fireSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWeaponComponent_Init_Statics::NewProp_m_bulletsPerMagazine,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWeaponComponent_Init_Statics::NewProp_m_bulletsInMagazineLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWeaponComponent_Init_Statics::NewProp_m_bulletsCurrent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWeaponComponent_Init_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWeaponComponent_Init_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWeaponComponent, nullptr, "Init", sizeof(WeaponComponent_eventInit_Parms), Z_Construct_UFunction_UWeaponComponent_Init_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UWeaponComponent_Init_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWeaponComponent_Init_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UWeaponComponent_Init_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWeaponComponent_Init()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWeaponComponent_Init_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWeaponComponent_Reload_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWeaponComponent_Reload_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWeaponComponent_Reload_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWeaponComponent, nullptr, "Reload", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWeaponComponent_Reload_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UWeaponComponent_Reload_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWeaponComponent_Reload()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWeaponComponent_Reload_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics
	{
		struct WeaponComponent_eventSpawnBullet_Parms
		{
			UCameraComponent* m_camera;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_camera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_camera;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WeaponComponent_eventSpawnBullet_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WeaponComponent_eventSpawnBullet_Parms), &Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::NewProp_m_camera_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::NewProp_m_camera = { "m_camera", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WeaponComponent_eventSpawnBullet_Parms, m_camera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::NewProp_m_camera_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::NewProp_m_camera_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::NewProp_m_camera,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWeaponComponent, nullptr, "SpawnBullet", sizeof(WeaponComponent_eventSpawnBullet_Parms), Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWeaponComponent_SpawnBullet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWeaponComponent_SpawnBullet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UWeaponComponent_NoRegister()
	{
		return UWeaponComponent::StaticClass();
	}
	struct Z_Construct_UClass_UWeaponComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_BulletActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_g_BulletActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_ThisWeaponPickableActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_g_ThisWeaponPickableActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_TimeSinceReload_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_g_TimeSinceReload;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_LastShotTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_g_LastShotTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_ReloadTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_g_ReloadTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_FireSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_g_FireSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_BulletsPerMagazine_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_g_BulletsPerMagazine;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_BulletsInMagazineLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt16PropertyParams NewProp_g_BulletsInMagazineLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_g_BulletsCurrent_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt16PropertyParams NewProp_g_BulletsCurrent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWeaponComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_KF,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UWeaponComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UWeaponComponent_CreatePickableWeapon, "CreatePickableWeapon" }, // 2793053119
		{ &Z_Construct_UFunction_UWeaponComponent_Fire, "Fire" }, // 3133578003
		{ &Z_Construct_UFunction_UWeaponComponent_Init, "Init" }, // 4205758027
		{ &Z_Construct_UFunction_UWeaponComponent_Reload, "Reload" }, // 3692212827
		{ &Z_Construct_UFunction_UWeaponComponent_SpawnBullet, "SpawnBullet" }, // 2610725643
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeaponComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "WeaponComponent.h" },
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletActor_MetaData[] = {
		{ "Category", "Weapons" },
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletActor = { "g_BulletActor", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWeaponComponent, g_BulletActor), Z_Construct_UClass_ABullet_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletActor_MetaData, ARRAY_COUNT(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_ThisWeaponPickableActor_MetaData[] = {
		{ "Category", "Weapons" },
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_ThisWeaponPickableActor = { "g_ThisWeaponPickableActor", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWeaponComponent, g_ThisWeaponPickableActor), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_ThisWeaponPickableActor_MetaData, ARRAY_COUNT(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_ThisWeaponPickableActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_TimeSinceReload_MetaData[] = {
		{ "Category", "Weapon" },
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_TimeSinceReload = { "g_TimeSinceReload", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWeaponComponent, g_TimeSinceReload), METADATA_PARAMS(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_TimeSinceReload_MetaData, ARRAY_COUNT(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_TimeSinceReload_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_LastShotTime_MetaData[] = {
		{ "Category", "Weapon" },
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_LastShotTime = { "g_LastShotTime", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWeaponComponent, g_LastShotTime), METADATA_PARAMS(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_LastShotTime_MetaData, ARRAY_COUNT(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_LastShotTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_ReloadTime_MetaData[] = {
		{ "Category", "Weapon" },
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_ReloadTime = { "g_ReloadTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWeaponComponent, g_ReloadTime), METADATA_PARAMS(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_ReloadTime_MetaData, ARRAY_COUNT(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_ReloadTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_FireSpeed_MetaData[] = {
		{ "Category", "Weapon" },
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_FireSpeed = { "g_FireSpeed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWeaponComponent, g_FireSpeed), METADATA_PARAMS(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_FireSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_FireSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsPerMagazine_MetaData[] = {
		{ "Category", "Weapon" },
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsPerMagazine = { "g_BulletsPerMagazine", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWeaponComponent, g_BulletsPerMagazine), METADATA_PARAMS(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsPerMagazine_MetaData, ARRAY_COUNT(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsPerMagazine_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsInMagazineLeft_MetaData[] = {
		{ "Category", "WeaponComponent" },
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt16PropertyParams Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsInMagazineLeft = { "g_BulletsInMagazineLeft", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWeaponComponent, g_BulletsInMagazineLeft), METADATA_PARAMS(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsInMagazineLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsInMagazineLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsCurrent_MetaData[] = {
		{ "Category", "WeaponComponent" },
		{ "ModuleRelativePath", "WeaponComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt16PropertyParams Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsCurrent = { "g_BulletsCurrent", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWeaponComponent, g_BulletsCurrent), METADATA_PARAMS(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsCurrent_MetaData, ARRAY_COUNT(Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsCurrent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWeaponComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_ThisWeaponPickableActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_TimeSinceReload,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_LastShotTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_ReloadTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_FireSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsPerMagazine,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsInMagazineLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeaponComponent_Statics::NewProp_g_BulletsCurrent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWeaponComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWeaponComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWeaponComponent_Statics::ClassParams = {
		&UWeaponComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UWeaponComponent_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UWeaponComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UWeaponComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UWeaponComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWeaponComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWeaponComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWeaponComponent, 2040534414);
	template<> KF_API UClass* StaticClass<UWeaponComponent>()
	{
		return UWeaponComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWeaponComponent(Z_Construct_UClass_UWeaponComponent, &UWeaponComponent::StaticClass, TEXT("/Script/KF"), TEXT("UWeaponComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWeaponComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
